var express = require('express');
var bodyParser = require('body-parser');
var socketFiles = require('socketio-file-upload');
var app = express().use(socketFiles.router);
var sqlite3 = require("sqlite3").verbose();
var path = require("path");

var fs = require("fs");
var server = require('http').createServer(app);
var io = require('socket.io', { rememberTransport: false, transports: ['WebSocket', 'Flash Socket', 'AJAX long-polling'] }).listen(server);

users = [];
connections = [];

var db = new sqlite3.Database(__dirname + '/Hermes.sqlite', error => {
    if (error)
        console.log(error.message);
});



server.listen(process.env.PORT || 3000);
console.log('Server running...');

app.use("/", express.static(__dirname + "/"))

//Estableciendo los middlewares
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));


//Eventos express
app.get('/', function (req, res) {
    res.sendFile(__dirname + '/paginaprincipal.html');
});


//Eventos sockets
io.sockets.on('connection', function (socket) {
    var uploader = new socketFiles();
    uploader.dir = path.join(__dirname, '/ImagenesUsuarios');
    uploader.listen(socket);


    //Conectados
    connections.push(socket);
    console.log('Conexion: %s sockets conectados', connections.length);

    //Desconectados
    socket.on('disconnect', function (data) {
        if (!socket.username)
            return;

        users.splice(users.indexOf(socket.username, 1));
        updateUserNames();
        connections.splice(connections.indexOf(socket), 1);
        console.log('Desconexion: %s sockets conectados', connections.length);

    });


    //Registrando usuarios 
    socket.on('registro', data => {
        var bloq = 0;
        var vic = 0; 
        var conv = 0; 
        var pun = 0;
        db.run('INSERT INTO user(NombreUsuario, eMail, Contrasena, RutaImagen, Mensaje, Bloques, Conversaciones, Victorias, Puntuacion ) VALUES(\'' + data.userName + '\',\'' + data.email + '\',\'' + data.password + '\',\'' + data.img + '\',\'' + data.mensaje + '\',\'' + bloq + '\',\'' + conv + '\',\'' + vic + '\',\'' + pun + '\')', error => {
            if (error) {
                io.to(data.socketId).emit('ErrorRegistro');
                return;
            }

            io.to(data.socketId).emit('ExitoRegistro');
        });
    });

    socket.on('mail', data=>{
        var remitente = searchSocketByName(data.orig);
        io.to(remitente).emit('write mail', { orig : data.orig, dst : data.dst });

    });
    socket.on('buscardest', data=>{
        var userId = searchUserIdByName(data.dst);
        var socketId = searchSocketByName(data.orig);
        var sql = "SELECT * FROM user WHERE UsuarioId = " + userId;
        db.all(sql, [], (error, rows) =>{
            if(error){
                console.error.mensaje
                return;
            }
            if (rows.length < 1){
                console.log("No se encontro el registro");
                return;
            }
            rows.forEach(row=>{
                io.to(socketId).emit('escribirdest', { email : row.eMail });
            });
        });
    });
   
    socket.on('send game', data => {
        var socketid = searchSocketByName(data.dst);
        io.to(socketid).emit('ask game', { orig : data.orig});
    });

    // socket.on('actualizarPerfil', data=>{
    //     var set = "SET Puntuacion =" + puntajeJ1;
    //     var sql = "UPDATE user " + set + " WHERE NombreUsuario = " + data.J1;
        
    //     db.run(sql, error =>{
    //         if(error){
    //             io.to(data.SJ1).emit('ErrorActualizacion');
    //         }
    //     })
    //     var set = "SET Puntuacion =" + puntajeJ2;
    //     var sql = "UPDATE user " + set + " WHERE NombreUsuario = " + data.J2;
        
    //     db.run(sql, error =>{
    //         if(error){
    //             io.to(data.SJ1).emit('ErrorActualizacion');
    //         }
    //     })
    // });

  socket.on('send video', data => {
        var socketid = searchSocketByName(data.dst);
        io.to(socketid).emit('ask video', { orig : data.orig});
    });

    socket.on('busca conversacion', data =>{
        var userId = searchUserIdByName(data.target);
        var sql = "SELECT * FROM user WHERE UsuarioId = " + userId;

        db.all(sql, [], (error, rows) => {
            if (error) {
                console.log(error.message);
                //io.to(data.socketId).emit('Error en log-in');
                return;
            }

            if (rows.length < 1) {
                console.log("No se encontro el registro");
                //io.to(data.socketId).emit('Error en log-in');
                return;
            }

            rows.forEach(row => {
                var socketId = searchSocketByName(data.orig);
                var usuario = searchUserByName(row.NombreUsuario);
                io.to(socketId).emit('retorna conversacion', { user: row.NombreUsuario, msg: row.Mensaje, img: row.RutaImagen, Estado: usuario.Estado});
            
            });
        });

    });


    //Agregando usuarios
    socket.on('log-In', function (data, callback) {
        //callback(true);
        //Aqui ira una pequeña logica de BD

        var sql = "SELECT * FROM user WHERE NombreUsuario =\'" + data.user + "\' AND Contrasena =\'" + data.password + "\'";

        db.all(sql, [], (error, rows) => {
            if (error) {
                console.log(error.message);
                io.to(data.socketId).emit('Error en log-in');
                return;
            }

            if (rows.length < 1) {
                console.log("No se encontro el registro");
                io.to(data.socketId).emit('Error en log-in');
                return;
            }

            rows.forEach(row => {
                socket.username = data.user;
                users.push({ user: socket.username, userId: row.UsuarioId , Estado: data.Estado, socketId: data.socketId });
                console.log("El usuario " + data.user + " se ha logueado");
                io.to(data.socketId).emit('Exito log-in', { user: row.NombreUsuario, msg: row.Mensaje, img: row.RutaImagen, email: row.eMail, puntuacion : row.Puntuacion, bloques : row.Bloques, victorias : row.Victorias, conver : row.Conversaciones });
                updateUserNames();
            });

        });
    });





    //Actualiza el estado de un usuario para todos los clientes
    socket.on('update state', data => {

        for (var i = 0; i < users.length; i++) {
            if (users[i].user == data.user)
                users[i].Estado = data.Estado;
        }
        updateUserNames();
    });

    socket.on('send message', data => {
        var dstSocket = searchSocketByName(data.dst);

        if (data.dst == "Todos") {
            for (var i = 0; i < users.length; i++) {
                if (users[i].user.trim() != data.orig.trim()) {
                    dstSocket = searchSocketByName(users[i].user);
                    io.to(dstSocket).emit('receive message', { orig: data.orig, message: data.msg, isGlobal: true });
                }
            }

            return;
        }

        if (dstSocket.length < 1)
            return;

        io.to(dstSocket).emit('receive message', { orig: data.orig, message: data.msg, isGlobal: false });
    });

    socket.on('actualiza', () => {
        updateUserNames();
    });

    socket.on('reiniciarAmigos', data =>{
        var usuarioID = searchUserIdByName(data.user);
        var sql = "UPDATE user SET Conversaciones = 0 WHERE UsuarioId = " + usuarioID;

        db.run(sql , error => {
            if (error) {
                console.log("Error Actualizar");
                console.log(error);
                return;
            }
            return;
        });

    });
    socket.on('actualizarAmigable', data=>{
        var usuarioID = searchUserIdByName(data.user);
        var SET = "UPDATE user SET Conversaciones = " + data.convers;
        var where = " WHERE UsuarioId = " + usuarioID;
        var sql = SET + where;

        db.run(sql , error => {
            if (error) {
                console.log("Error Actualizar");
                console.log(error);
                return;
            }

            io.to(data.socket).emit('actCompletaAmigable', { nombre : data.user });
        });

    });

    socket.on('buscarPerfil', data=>{
        var usuarioID = searchUserIdByName(data.user);
        var socket = searchSocketByName(data.user);
        var sql = "SELECT * FROM user WHERE UsuarioId = " + usuarioID;

        db.all(sql, [], (error, rows) =>{
            if(error){
                console.error.mensaje
                return;
            }
            if (rows.length < 1){
                console.log("No se encontro el registro");
                return;
            }
            rows.forEach(row=>{
                 io.to(socket).emit('actuaalizarPerfil', { puntuacion : row.Puntuacion, bloques : row.Bloques, victorias : row.Victorias, conver : row.Conversaciones });
            });
        });

    });

    socket.on('actualizaPuntaje', data =>{
        var usuarioID = searchUserIdByName(data.user);
        var SET= "UPDATE user SET Puntuacion = Puntuacion + " + data.puntaje;
        var where = " WHERE UsuarioId = " + usuarioID;
        var sql = SET + where;

        db.run(sql , error => {
            if (error) {
                console.log("Error Actualizar");
                console.log(error);
                return;
            }

            return;
        });
    });
    
    socket.on('actualizarVictoriaa', data =>{
        var usuarioID = searchUserIdByName(data.user);
        var sql = "UPDATE user SET Victorias = Victorias + 1 WHERE UsuarioId = " + usuarioID;

        db.run(sql , error => {
            if (error) {
                console.log("Error Actualizar");
                console.log(error);
                return;
            }
            return;
        });

    });
    socket.on('actualizaBloques', data =>{
        var usuarioID = searchUserIdByName(data.user);
        var SET= "UPDATE user SET Bloques = Bloques + " + data.bloques;
        var where = " WHERE UsuarioId = " + usuarioID;
        var sql = SET + where;

        db.run(sql , error => {
            if (error) {
                console.log("Error Actualizar");
                console.log(error);
                return;
            }
            return;
        });
    });

    socket.on('accept game', data => {
        var socketPlayer2 = searchSocketByName(data.dst);
        var socketPlayer1 = searchSocketByName(data.orig);

        if (socket.length < 1)
            return;

        io.to(socketPlayer1).emit('begin game', { SUsuario : socketPlayer1, player: data.dst, playerNum: 1, socketJ1 : socketPlayer1, socketJ2 : socketPlayer2, player1 : data.dst, player2 : data.orig });
        io.to(socketPlayer2).emit('begin game', { SUsuario : socketPlayer2, player: data.orig, playerNum: 2, socketJ1 : socketPlayer1, socketJ2 : socketPlayer2, player1 : data.dst, player2 : data.orig });
    });

    socket.on('accept video', data => {
        var socketPlayer2 = searchSocketByName(data.dst);
        var socketPlayer1 = searchSocketByName(data.orig);

        if (socket.length < 1)
            return;

        io.to(socketPlayer1).emit('begin video', { player: data.dst, playerNum: 1 });
        io.to(socketPlayer2).emit('begin video', { player: data.orig, playerNum: 2 });
    });

    socket.on('zumbido', data =>{
        var socketID = searchSocketByName(data.dst);
        io.to(socketID).emit('zumbido', data.org);
    });

        //socket
        socket.on('ColorRandom', data =>{
            if (data.color == 0) {
                io.to(data.socketJ1).emit('especialesColor', { especialColor : "green"}); 
                io.to(data.socketJ2).emit('especialesColor', { especialColor : "green"}); 

            }
            if (data.color == 1) {
                io.to(data.socketJ1).emit('especialesColor', { especialColor : "yellow"}); 
                io.to(data.socketJ2).emit('especialesColor', { especialColor : "yellow"}); 
            }
            if (data.color == 2) {
                io.to(data.socketJ1).emit('especialesColor', { especialColor : "pink"}); 
                io.to(data.socketJ2).emit('especialesColor', { especialColor : "pink"}); 
            } 
        });

    socket.on('update position', data => {
        var socketId = searchSocketByName(data.player);
        io.to(socketId).emit('update enemy', { pos: data.pos, playerNum: data.playerNum });
    });

    function updateUserNames() {
        io.sockets.emit('get users', users);
    }

    function searchSocketByName(userName) {
        for (var i = 0; i < users.length; i++) {
            if (users[i].user == userName)
                return users[i].socketId;
        }

        return "";
    }

    function searchUserIdByName(userName) {
        for (var i = 0; i < users.length; i++) {
            if (users[i].user == userName)
                return users[i].userId;
        }

        return "";
    }

    function searchUserByName(userName) {
        for (var i = 0; i < users.length; i++) {
            if (users[i].user == userName)
                return users[i];
        }

        return undefined;
    }
});

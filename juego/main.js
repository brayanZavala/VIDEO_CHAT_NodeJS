

var ctx;

// CREAR VARIABLES 
var timer = 0;
var contador = 0;
var hitcounter = 0;
var velocidad = 3;
var direccion = velocidad;
var velocidad2 = 3;
var direccion2 = velocidad2;
var entra = true;
var dx, dy;
var puntajeJ1 = 0, puntajeJ2 = 0;
var bloquesJ1 = 0, bloquesJ2 = 0;
var ganaste = false;
var victoria = false;
var delay = true;
var duno = false;
var ddos = false; 
var dtres = false; 
var ya = false;
var actualizarpuntaje = false;
//DELAY
var textoDelay = {
    uno : "1",
    dos : "2", 
    tres : "3", 
    ya : "COMIENZA"
}
var timerdelay = 0;
var contadordelay = 0;
//VARIABLES JSON
var textoRespuesta = {
    titulo: " ",
    subtitulo1: "",
    subtitulo2: ""
}

var bloqueJ1 = {
    x: 100,
    y: 0,
    width: 60,
    height: 20
}
var bloqueJ2 = {
    x: 100,
    y: 0,
    width: 60,
    height: 20
}
var juego = {
    estado: "iniciando"
}
var bolita = {
    x: 200,
    y: 200,
    width: 5,
    height: 5,
    color: "J1",
    estado: "vivo"
}
var teclado = {};
//variables para las imagenes
var fondo;

//DEFINICION de FUNCIONES
function loadMedia() {
    fondo = new Image();
    fondo.src = "imagenes/fondojuego.jpg";
    fondo.onload = function () {
        var intervalo = window.setInterval(frameLoop, 1000 / 55);
    }
    bloqueJ2.y = canvas.height - 350;
    bloqueJ1.y = canvas.height - 50;
}
function dibujabolita() {
    if (bolita.estado == "vivo") {
        ctx.save();
        if (bolita.color == "J1") {
            ctx.fillStyle = "blue";
        }
        if (bolita.color == "J2") {
            ctx.fillStyle = "red";
        }
        ctx.arc(bolita.x, bolita.y, 10, 0, 7);
        ctx.fill();
        ctx.restore();
    }
}
function dibujarEspeciales() {
    ctx.save();
    if (especiales.estado == "vivo") {
        ctx.fillStyle = especiales.color;
        ctx.fillRect(especiales.x, especiales.y, especiales.width, especiales.height);
        ctx.restore();
    }
}

function dibujarBloqueJ2() {
    ctx.save();
    ctx.fillStyle = "red";
    ctx.fillRect(bloqueJ2.x, bloqueJ2.y, bloqueJ2.width, bloqueJ2.height);
    ctx.restore();
}
function dibujaFondo() {
    ctx.drawImage(fondo, 0, 0);
}
function dibujaBloqueJ1() {
    ctx.save();
    ctx.fillStyle = "blue";
    ctx.fillRect(bloqueJ1.x, bloqueJ1.y, bloqueJ1.width, bloqueJ1.height);
    ctx.restore();
}
function hit(a, b) {
    var hit = false;
    if (b.x + b.width >= a.x && b.x < a.x + a.width) {
        if (b.y + b.height >= a.y && b.y < a.y + a.height) {
            hit = true;
        }
    }
    if (b.x <= a.x && b.x + b.width >= a.x + a.width) {
        if (b.y <= a.y && b.y + b.height >= a.y + a.height) {
            hit = true;
        }
    }
    if (a.x <= b.x && a.x + a.width >= b.x + b.width) {
        if (a.y <= b.y && a.y + a.height >= b.y + b.height) {
            hit = true;
        }
    }
    return hit;
}
function verificarColision() {
    if (juego.estado == "iniciando") {
        juego.estado = "jugando";
        console.log(juego.estado);
        return;
    }
    if (hitcounter > 0) {
        hitcounter++;
        console.log("delay colision")
        if (hitcounter >= 50) {
            hitcounter = 0;
        }
        return;
    }
    if (hit(bolita, especiales)) {
        console.log("colision")
        especiales.estado = "muerto";
        especiales.contador = 0;
        especiales.x = 200;
        especiales.y = 200;
        if (bolita.color == "J1") {
            bloquesJ1++;
            console.log(bloquesJ1);
            if (especiales.color == "green") { puntajeJ1 += 10; }
            else if (especiales.color == "pink") { puntajeJ1 += 20; }
            else if (especiales.color == "yellow") { puntajeJ1 += 50; }
        }
        if (bolita.color == "J2") {
            bloquesJ2++;
            console.log(bloquesJ2);
            if (especiales.color == "green") puntajeJ2 += 10;
            else if (especiales.color == "pink") puntajeJ2 += 20;
            else if (especiales.color == "yellow") puntajeJ2 += 50;
            console.log("puntajeJ2 " + puntajeJ2);

        }
        hitcounter++;
    }
}
function ColisionBloqueJ1() {
    if (hit(bolita, bloqueJ1)) {
        direccion = -direccion;
        bolita.color = "J1";
    }
}
function ColisionBloqueJ2() {
    if (hit(bolita, bloqueJ2)) {
        direccion = -direccion;
        bolita.color = "J2";
    }
}
//TECLAAADOOO
function agregarEventosTeclado() {
    agregarEvento(document, "keydown", function (e) {
        teclado[e.keyCode] = true;
    });
    agregarEvento(document, "keyup", function (e) {
        teclado[e.keyCode] = false;
    });

    function agregarEvento(elemento, nombreEvento, funcion) {
        if (elemento.addEventListener) {
            //NAVEGADORES de verdad 
            elemento.addEventListener(nombreEvento, funcion, false);
        }
        else if (elemento.attachEvent) {
            //internet explorer
            elemento.attachEvent(nombreEvento, funcion);
        }
    }
}

//ACTUALIZAAAR
function actualizaBolita() {

    if (juego.estado == "jugando") {
        if(bolita.estado == "muerto"){
            bolita.y = 100;
        }
        if (bolita.estado == "vivo") {
            canvas.width = canvas.width;
            if (bolita.y > (canvas.height)) {
                direccion = -velocidad;
            }
            if (bolita.y < 0) {
                direccion = velocidad;
            }

            bolita.y += direccion;

            if (bolita.x > (canvas.width)) {
                direccion2 = -velocidad2;
            }
            if (bolita.x < 10) {
                direccion2 = velocidad2;
            }
            bolita.x += direccion2;

            if (bolita.y <= 0) {
                juego.estado = "terminado";
                bolita.estado = "muerto";
                actualizarpuntaje = true;
                if(numeroJugador == 1){
                    ganaste = true;
                }
                if(numeroJugador == 2){
                    ganaste = false;
                }

            }
            if (bolita.y >= canvas.height) {
                juego.estado = "terminado";
                bolita.estado = "muerto";
                actualizarpuntaje = true;
                if(numeroJugador == 1){
                    ganaste = true;
                }
                if(numeroJugador == 2){
                    ganaste = false;
                }            
            }
        }
    }
}
function elegircolor() {
    Random();
}

function actualizaEspeciales() {
    if (especiales.estado == "vivo") {
        especiales.contador++;
        var ida = true;
        especiales.x += Math.sin(especiales.contador * Math.PI / 90) * 10;
        especiales.y += Math.cos(especiales.contador * Math.PI / 90) * 2;
    }
    if (especiales.estado == "muerto") {
        timmer();
    }
    if (especiales.estado == "muerto" && timer >= 5) {
        elegircolor();
        especiales.estado = "vivo";
        timer = 0;
        entra = true;
    }

}
function moverBloqueJ2() {

    if(numeroJugador != 2)
        return;

    if (teclado[37] && numeroJugador == 2) {
        bloqueJ2.x -= 10;
        if (bloqueJ2.x < 0) bloqueJ2.x = 0;
    }
    if (teclado[39] && numeroJugador == 2) {
        var limite = canvas.width - bloqueJ2.width;
        bloqueJ2.x += 10;
        if (bloqueJ2.x > limite) bloqueJ2.x = limite;
    }

    socket.emit("update position",{player: nombrejugador, pos: bloqueJ2.x, playerNum: numeroJugador});

}
function moverbloqueJ1() {

    if(numeroJugador != 1)
        return;

    if (teclado[37] && numeroJugador == 1) {
        bloqueJ1.x -= 10;
        if (bloqueJ1.x < 0) bloqueJ1.x = 0;
    }
    if (teclado[39] && numeroJugador == 1) {
        var limite = canvas.width - bloqueJ1.width;
        bloqueJ1.x += 10;
        if (bloqueJ1.x > limite) bloqueJ1.x = limite;
    }

    socket.emit("update position",{player: nombrejugador, pos: bloqueJ1.x, playerNum: numeroJugador});
}

function dibujaTexto() {
    ctx.save();
    if(juego.estado == "jugando"){
        ctx.fillStyle = "white";
        ctx.font = "20px Arial"; 
        ctx.fillText("Puntaje J1 = " + puntajeJ1, 10, 20);
        ctx.fillText("Puntaje J2 = "+ puntajeJ2, 300, 20);

    }
    if (juego.estado == "terminado") {

        if(actualizarpuntaje == true){
            ActualizaPuntajee(puntajeJ1, puntajeJ2);
            ActualizaBloques(bloquesJ1, bloquesJ2);
            actualizarpuntaje = false;
            if(ganaste==true){
                ActualizarVictoria();
            }
        }
        if (ganaste == true) {
            ctx.fillStyle = "white";
            ctx.font = "Bold 40px Arial";
            ctx.fillText(textoRespuesta.titulo, 200, 200);
            ctx.font = "20px Arial";
            ctx.fillText(textoRespuesta.subtitulo1, 100, 300);
            ctx.fillText(textoRespuesta.subtitulo2, 300, 300);
        }
        if (ganaste == false) {
            ctx.fillStyle = "red";
            ctx.font = "Bold 40px Arial";
            ctx.fillText(textoRespuesta.titulo, 200, 200);
            ctx.font = "20px Arial";
            ctx.fillText(textoRespuesta.subtitulo1, 100, 300);
            ctx.fillText(textoRespuesta.subtitulo2, 300, 300);
        }

    }
}
function actualizarEstadoJuego(gane) {
    if (juego.estado == "terminado") {
        textoRespuesta.subtitulo1 = "Puntuacion J1 = "+ puntajeJ1;
        textoRespuesta.subtitulo2 = "Puntuacion J2 = "+ puntajeJ2;

        if (ganaste == true) {
            textoRespuesta.titulo = "¡GANASTE!";
        }
        if (ganaste == false) {
            textoRespuesta.titulo = "¡PERDISTE";
        }
    }
}
function timmer() {
    contador++;
    if (contador >= 50) {
        contador = 0;
        timer++;
    }
}


////////DELAY 
function timmerdelay(){
    contadordelay++;
    if(contadordelay >=50){
        contadordelay = 0;
        timerdelay++;
    }
}
function actualizaTextoDelay(){
    if(timerdelay >0 && timerdelay <= 2){        
        duno = true; 
    }
    if(timerdelay >2 && timerdelay <=4){
        ddos = true; 
        duno = false; 
    }
    if (timerdelay >4 && timerdelay <=6){
        ddos = false; 
        dtres = true; 
    }
    if (timerdelay >6 && timerdelay <=8){
        ya = true; 
        dtres = false;
    }
    if (timerdelay >8 && timerdelay <=10){
        delay =false;
        ya = false;
    }
}
function dibujartextoDelay(){
    ctx.save();
    ctx.fillStyle = "white";
    ctx.font = "100px Arial";
if(duno == true){
    ctx.fillText(textoDelay.uno, 310, 200);    
}
if(ddos == true){
    ctx.fillText(textoDelay.dos, 310, 200);    
}
if(dtres == true){
    ctx.fillText(textoDelay.tres, 310, 200);    
}
if(ya == true){
    ctx.fillText(textoDelay.ya, 200, 200);    
}
}
//LOOOOP
function frameLoop() {
    if(delay == true){
        dibujaFondo();
        actualizaTextoDelay();
        dibujartextoDelay();
        timmerdelay();
    }
    else {
    actualizarEstadoJuego();
    moverbloqueJ1();
    moverBloqueJ2();
    actualizaEspeciales();
    actualizaBolita();
    dibujaFondo();
    verificarColision();
    ColisionBloqueJ2();
    ColisionBloqueJ1();
    dibujarEspeciales();
    dibujaTexto();
    dibujaBloqueJ1();
    dibujarBloqueJ2();
    dibujabolita();
    }
}
